# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# bread = Category.create(name: "Bread")
# bagel = Category.create(name: "Bagel")
# pie = Category.create(name: "Pie")
# cookie = Category.create(name: "Cookie")
# pastry = Category.create(name: "Pastry")

Province.create(name: "Alberta", gst: 0.05)
Province.create(name: "British Columbia", pst: 0.07, gst: 0.05)
Province.create(name: "Manitoba", pst: 0.08, gst: 0.05)
Province.create(name: "New Brunswick", hst: 0.13)
Province.create(name: "Newfoundland and Labrador", hst: 0.13)
Province.create(name: "Northwest Territories", gst: 0.05)
Province.create(name: "Nova Scotia", hst: 0.15)
Province.create(name: "Nunavut", gst: 0.05)
Province.create(name: "Ontario", hst: 0.13)
Province.create(name: "Prince Edward Island", hst: 0.14)
Province.create(name: "Quebec", pst: 0.09975, gst: 0.05)
Province.create(name: "Saskatchewan", pst: 0.05, gst: 0.05)
Province.create(name: "Yukon Territory", gst: 0.05)

# Product:
#   id:             integer
#     name:           string
#     description:    string
#     price:          decimal
#   stock_quantity: integer
#   created_at:     datetime
#   updated_at:     datetime
# Product.create(name: "", description: "", price: )
