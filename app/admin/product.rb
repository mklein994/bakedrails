ActiveAdmin.register Product do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  permit_params :name, :description, :price, :stock_quantity, :category_id, :image_file_name, :image_content_type, :image_file_size
  
  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :name
      f.input :description
      f.input :price
      f.input :stock_quantity
      # CONTINUE HERE! 12/01/14 12:53:56 PM
      # f.input :category, as: :select, collection: @
      f.input :image
      f.submit
    end
  end

  # Product info:
    # id: 1,
    # name: "Rye Bread",
    # description: "A loaf of tasty bread made from rye.",
    # price:
    # #BigDecimal:6e76e88,'0.175E1',18(45)>,
    # stock_quantity: 20,
    # created_at: "2014-11-27 20:37:30",
    # updated_at: "2014-11-28 01:58:28",
    # category_id: 11,
    # image_file_name: nil,
    # image_content_type: nil,
    # image_file_size: nil,
    # image_updated_at: nil
end
