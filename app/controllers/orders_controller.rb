class OrdersController < InheritedResources::Base

  private

    def order_params
      params.require(:order).permit(:status, :customer_id, :pst_rate, :gst_rate, :hst_rate)
    end
end

