class ProductsController < InheritedResources::Base

  def index
    @products = Product.all
  end

  private

    def product_params
      params.require(:product).permit(:name, :description, :category_id, :price, :stock_quantity)
    end
end

