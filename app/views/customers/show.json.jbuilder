json.extract! @customer, :id, :first_name, :last_name, :address, :city, :province_id, :country, :postal_code, :email, :created_at, :updated_at
